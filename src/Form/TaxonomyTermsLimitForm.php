<?php

namespace Drupal\taxonomy_terms_limit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Taxonomy Terms LimitForm.
 */
class TaxonomyTermsLimitForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(RouteMatchInterface $current_route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRouteMatch = $current_route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_terms_limit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $vid = $this->currentRouteMatch->getParameter("taxonomy_vocabulary");
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
    $limit = $vocabulary->getThirdPartySetting('taxonomy_terms_limit', 'limit');

    $form['limit'] = [
      '#type' => 'textfield',
      '#title' => 'Limit value',
      '#description' => $this->t('Add limit applicable for this taxonomy. Use -1 for unlimited.'),
      '#required' => FALSE,
      '#default_value' => $limit,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $limit = $form_state->getValue('limit');
    $vid = $this->currentRouteMatch->getParameter("taxonomy_vocabulary");
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vid);
    $vocabulary->setThirdPartySetting('taxonomy_terms_limit', 'limit', $limit);
    $vocabulary->save();
    $this->messenger()->addStatus($this->t('Limit value saved successfully!'));
  }

}
