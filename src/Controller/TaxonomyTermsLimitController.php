<?php

namespace Drupal\taxonomy_terms_limit\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatch;

/**
 * Builds an example page.
 */
class TaxonomyTermsLimitController {

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Drupal\Core\Routing\RouteMatch $route_match
   *   Get route match.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, RouteMatch $route_match) {
    $vid = $route_match->getParameter('taxonomy_vocabulary');
    return AccessResult::allowedIf($account->hasPermission('limit terms in ' . $vid));
  }

}
